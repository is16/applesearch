/* eslint-disable no-unused-vars */
import React, { useState } from 'react';
import { Container, Grid, CircularProgress } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import styled from 'styled-components';
import Search from './Search';
import Result from './Result';
import Option from './Option';

const TextItem = styled.div`
  text-align: left;
`;

const useStyle = makeStyles(() => ({
  circular: {
    margin: '0 auto',
    display: 'block',
    position: 'relative',
    top: '7%',
  },
}));

export default () => {
  const [descriptor, setDescriptor] = useState('ce');
  const [accuracy, setAccuracy] = useState(0.33);
  const [isJsonView, setIsJsonView] = useState(false);
  const style = useStyle();
  return (
    <Container>
      <Grid container spacing={3} justify="center">
        <Grid item xs={8}>
          <TextItem>
            <h1
              style={{
                fontFamily: 'San-Francisco',
              }}
            >
               Apple Search
            </h1>
          </TextItem>
          <Search />
          <Option
            setDescriptor={setDescriptor}
            setAccuracy={setAccuracy}
            isJsonView={isJsonView}
          />
        </Grid>
        <Grid item xs={7}>
          <React.Suspense
            fallback={<CircularProgress className={style.circular} />}
          >
            <Result isJsonView={isJsonView} setIsJsonView={setIsJsonView} />
          </React.Suspense>
        </Grid>
      </Grid>
    </Container>
  );
};
