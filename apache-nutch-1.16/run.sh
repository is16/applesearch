#!/bin/bash

# # Run in debug mode

if [[ -z "$JAVA_HOME" ]]; then
    JAVA_HOME="/usr/lib/jvm/default"
fi

## Remove crawl dbs
# [[ -d crawl ]] && rm -rf crawl

# # Remove all data in solr
curl "http://localhost:8983/solr/products/update?commit=true" \
    -H "Content-Type: text/xml" \
    --data-binary '<delete><query>*:*</query></delete>'

# rm -rf logs

# bin/nutch sitemap crawl/crawldb -sitemapUrls urls

bin/crawl -i -s urls crawl 10

# for segment in crawl/segments/*/; do
# 	rm -rf "$segment"/*parse*
#  	bin/nutch parse "$segment"
#     bin/nutch invertlinks crawl/linkdb -dir crawl/segments
# 	bin/nutch dedup crawl/crawldb/
# 	bin/nutch index crawl/crawldb/ -linkdb crawl/linkdb/ "$segment"
# done
