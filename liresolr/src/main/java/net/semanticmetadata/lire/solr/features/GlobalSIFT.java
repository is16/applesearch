package net.semanticmetadata.lire.solr.features;

import net.semanticmetadata.lire.imageanalysis.features.LireFeature;
import net.semanticmetadata.lire.imageanalysis.features.GlobalFeature;
import net.semanticmetadata.lire.utils.SerializationUtils;
import net.semanticmetadata.lire.utils.ImageUtils;
import net.semanticmetadata.lire.solr.features.opencv.*;

import org.opencv.core.CvType;
import org.opencv.core.Mat;

import java.awt.image.BufferedImage;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.List;

import org.opencv.core.Core;
import org.opencv.features2d.DescriptorMatcher;
import org.opencv.core.MatOfDMatch;
import org.opencv.core.DMatch;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;

// class FileWriter {
//     public static void log(String filename) {
//         try (java.io.PrintWriter w = new java.io.PrintWriter(new java.io.File(filename))) {
//             w.write(Core.NATIVE_LIBRARY_NAME + "\n");
//             w.write(System.getProperty("java.library.path") + "\n");
//             w.write(java.time.LocalTime.now().toString() + "\n");
//             w.write(new java.io.File(GlobalSIFT.class.getProtectionDomain().getCodeSource().getLocation().toURI())
//                     .getPath() + "\n");
//         } catch (Exception e) {
//         }
//     }
// }

public class GlobalSIFT implements GlobalFeature {
    private double[] feature = null;
    CvSiftExtractor extractor;
    private final int DESCRIPTOR_SIZE = 128;
    private float ratioThresh = 0.7f;

    //#region Sift config
    // Limit maximum number of keypoints = nfeature + 1
    private int nfeatures = 240;
    // Default argument
    //#endregion

    //#region EMD
    int distType = Imgproc.CV_DIST_L2;
    //#endregion

    static {
        nu.pattern.OpenCV.loadShared();
        System.loadLibrary(Core.NATIVE_LIBRARY_NAME);
        // FileWriter.log("/home/hyperion/opencv.log");
    }

    public GlobalSIFT() {
    }

    @Override
    public void extract(BufferedImage img) {
        BufferedImage bimg = ImageUtils.createWorkingCopy(img);
        extractor = new CvSiftExtractor(nfeatures);
        extractor.extract(bimg);

        LinkedList<CvSiftFeature> extractedFeatures = extractor.getFeatures();

        feature = new double[extractedFeatures.size() * DESCRIPTOR_SIZE];
        int idx = 0, i = 0;
        double[] row;
        for (CvSiftFeature feat : extractedFeatures) {
            row = feat.getFeatureVector();
            for (i = 0; i < row.length; i++) {
                feature[idx] = row[i];
                idx++;
            }
        }
    }

    @Override
    public double[] getFeatureVector() {
        return feature;
    }

    @Override
    public double getDistance(LireFeature vd) {
        if (!(vd instanceof GlobalSIFT)) {
            throw new UnsupportedOperationException("Wrong descriptor.");
        }
        GlobalSIFT ch = (GlobalSIFT) vd;
        return getElectionDistance(getDescriptors(), ch.getDescriptors(), ratioThresh);

        // return getEarthMoverDistance(getDescriptors(), ch.getDescriptors(), distType);
    }

    private Mat getDescriptors() {
        final int rows = feature.length / DESCRIPTOR_SIZE;
        // return new Mat(ROWS, DESCRIPTOR_SIZE, CvType.CV_32FC1, new Scalar(feature));
        Mat descriptors = new Mat(rows, DESCRIPTOR_SIZE, CvType.CV_32FC1);
        for (int row = 0; row < rows; row++) {
            for (int col = 0; col < DESCRIPTOR_SIZE; col++) {
                descriptors.put(row, col, feature[row * DESCRIPTOR_SIZE + col]);
            }
        }
        return descriptors;
    }

    private static double getElectionDistance(Mat descriptor1, Mat descriptor2, float ratioThresh) {
        DescriptorMatcher matcher = DescriptorMatcher.create(DescriptorMatcher.FLANNBASED);
        List<MatOfDMatch> knnMatches = new ArrayList<>();

        matcher.knnMatch(descriptor1, descriptor2, knnMatches, 2);
        // -- Filter matches using the Lowe's ratio test
        int numberOfGoodMatches = 0;
        for (int i = 0; i < knnMatches.size(); i++) {
            if (knnMatches.get(i).rows() > 1) {
                DMatch[] matches = knnMatches.get(i).toArray();
                if (matches[0].distance < ratioThresh * matches[1].distance) {
                    numberOfGoodMatches++;
                }
            }
        }
        return 1.0 / ((double) numberOfGoodMatches + 1.0d);
    }

    private static double getEarthMoverDistance(Mat descriptor1, Mat descriptor2, int distType) {
        return (double)Imgproc.EMD(descriptor1, descriptor2, distType);
    }

    @Override
    public byte[] getByteArrayRepresentation() {
        return SerializationUtils.toByteArray(feature);
    }

    @Override
    public void setByteArrayRepresentation(byte[] in) {
        feature = SerializationUtils.toDoubleArray(in);
    }

    @Override
    public void setByteArrayRepresentation(byte[] in, int offset, int length) {
        feature = SerializationUtils.toDoubleArray(in, offset, length);
    }

    @Override
    public String getFeatureName() {
        return "globalSIFT";
    }

    @Override
    public String getFieldName() {
        return "GLOBAL_SIFT";
    }
}
