# AppleSearch

## Usage

Chế độ query mặc định của Solr là Boolean query.

1. Toán tử mặc định giữa các toán tử là OR: `iphone 5`.

2. Để tìm kiếm chính xác cụm từ, sử dụng dấu nháy kép: `"iphone 5"`.

3. Có thể kết hợp các cụm từ để tìm kiếm:

- `"iphone 5" OR "iphone 7"`: Tìm iphone 5 và iphone 7
- `"iphone 5" AND black`: Tìm iphone 5 màu đen.

4. Có thể loại bỏ một từ khỏi KQ tìm kiếm: `"iphone 6" AND NOT plus` (Tìm iphone 6, loại bỏ iphone 6 plus).

5. Có thể sử dụng wildcard: `{!complexphrase}"iphone 5*"`
