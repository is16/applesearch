/* eslint-disable react/no-array-index-key */
import React, { useState, useEffect } from 'react';
import { useParams } from 'react-router-dom';
import { getDataText } from 'api/solr';
import {
  List,
  ListItem,
  ListItemText,
  Divider,
  Typography,
  Container,
  Grid,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useRecoilValue } from 'recoil';
import { collectionState } from '../../recoil/atom';

const useStyle = makeStyles((theme) => ({
  root: {
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
  textField: {
    fontWeight: 600,
  },
}));

const ignoredFields = new Set(['id', '_version_']);

export default () => {
  const { id } = useParams();
  const [detailData, setDetailData] = useState(null);
  const collection = useRecoilValue(collectionState);
  const style = useStyle();

  useEffect(() => {
    const fetchDetailDevice = async () => {
      const data = await getDataText(collection, {
        query: `id:${id}`,
      });
      return data.response.docs[0];
    };

    fetchDetailDevice()
      .then((data) => {
        setDetailData(data);
      })
      .catch((e) => {
        console.log(e);
      });
  }, [id, collection]);
  return (
    detailData && (
      <Container>
        <List className={style.root}>
          {Object.entries(detailData).map(([key, value], index) =>
            !ignoredFields.has(key) ? (
              <React.Fragment key={index}>
                <ListItem>
                  <Grid container>
                    <Grid item xs={3}>
                      <ListItemText
                        primary={
                          <Typography
                            component="span"
                            variant="body2"
                            color="textPrimary"
                            display="block"
                          >
                            <span>{key}</span>
                          </Typography>
                        }
                      />
                    </Grid>
                    <Grid item xs={9}>
                      {key !== 'url' ? (
                        <ListItemText
                          primary={
                            <Typography
                              component="span"
                              variant="body2"
                              color="textPrimary"
                              display="block"
                            >
                              <span className={style.textField}>{value}</span>
                            </Typography>
                          }
                        />
                      ) : (
                        <a href={value}>Link</a>
                      )}
                    </Grid>
                  </Grid>
                </ListItem>
                <Divider />
              </React.Fragment>
            ) : null
          )}
        </List>
      </Container>
    )
  );
};
