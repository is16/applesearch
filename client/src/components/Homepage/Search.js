import React from 'react';
import { useRecoilState } from 'recoil';
import { TextField, InputAdornment } from '@material-ui/core';
import SearchIcon from '@material-ui/icons/Search';
import { debounce } from 'lodash';
import { queryParamState } from '../../recoil/atom';

export default () => {
  const [queryParam, setQueryParam] = useRecoilState(queryParamState);

  const handleInputChange = (event) => {
    // debounceSearch(event.target.value);
    if (event.keyCode === 13) {
      debounceSearch(event.target.value);
    }
  };

  const debounceSearch = debounce((query) => {
    const newQueryParam = { ...queryParam };
    newQueryParam.query = query;
    newQueryParam.page = 0;
    newQueryParam.rowsPerPage = 10;
    setQueryParam(newQueryParam);
  }, 500);

  return (
    <>
      <TextField
        id="outlined-start-adornment"
        placeholder="Enter name or URL of product..."
        InputProps={{
          startAdornment: (
            <InputAdornment position="start">
              <SearchIcon />
            </InputAdornment>
          ),
        }}
        fullWidth
        variant="outlined"
        onKeyDown={handleInputChange}
      />
    </>
  );
};
