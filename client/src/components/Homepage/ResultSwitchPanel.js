import React from 'react';
import {
  Switch, Grid,
} from '@material-ui/core';
import styled from 'styled-components';

const SwitchContainer = styled.div`
  text-align: right;
`;

export default function ResultSwitchPanel(props) {
  const { isJsonView, handleSwitchChange } = props;
  return (
    <Grid
      container
      direction="row"
      justify="flex-end"
      alignItems="baseline"
    >
      <Grid item>
        <SwitchContainer className="switch">
          <span>Table view</span>
          <Switch
            checked={isJsonView}
            onChange={handleSwitchChange}
            name="switchView"
          />
          <span>Json view</span>
        </SwitchContainer>
      </Grid>
    </Grid>
  );
}
