import React from 'react';
import {
  BrowserRouter as Router, Switch, Route,
} from 'react-router-dom';

import Homepage from './Homepage';
import Detail from './Detail';

function App() {
  return (
    <div className="App">
      <Router>
        <Switch>
          <Route exact path="/" component={Homepage} />
          <Route path="/detail/:id" component={Detail}></Route>
        </Switch>
      </Router>
    </div>
  );
}

export default App;
