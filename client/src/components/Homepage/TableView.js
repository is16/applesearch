import React from 'react';
import { List } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useRecoilValue } from 'recoil';
import { resultQuery, modeState } from '../../recoil/selector';
import ImageCell from './ImageCell';
import TextCell from './TextCell';

const useStyle = makeStyles((theme) => ({
  root: {
    width: '100%',
    backgroundColor: theme.palette.background.paper,
  },
}));

export default () => {
  const style = useStyle();
  const result = useRecoilValue(resultQuery);
  const mode = useRecoilValue(modeState);
  const { docs } = result;

  return (
    <List className={style.root}>
      {docs.map((row) => {
        let cell = null;
        if (mode === 'text') {
          cell = <TextCell row={row} key={row.id} />;
        } else {
          cell = <ImageCell row={row} key={row.id} />;
        }
        return <React.Fragment key={row.id}>{cell}</React.Fragment>;
      })}
    </List>
  );
};
