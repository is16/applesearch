import sys
import json
from operator import itemgetter

import ray
import requests


IMAGES = {
    "macbook": [
        "https://cnet3.cbsistatic.com/img/yjrw7VgWV7a95AvK8Ym0Np4bFXY=/1200x675/2017/06/27/13484418-bfd9-41e2-8f2d-9b4afb072da8/apple-macbook-pro-15-inch-2017-14.jpg",
        "https://www.anphatpc.com.vn/media/product/31270_laptop_apple_macbook_pro_16_inch_mvvj2saa_1.jpg",
        "https://images-na.ssl-images-amazon.com/images/I/61aOa%2B4v4IL._AC_SX466_.jpg",
        "https://cdn.tgdd.vn/Products/Images/44/220173/apple-macbook-air-2020-gold-1-600x600.jpg",
        "https://i.pcmag.com/imagery/reviews/038Dr5TVEpwIv8rCljx6UcF-14..v_1588802180.jpg",
        "https://bizweb.dktcdn.net/thumb/1024x1024/100/318/659/products/image-11-20-19-at-10-03-am-a10b2afc-8080-41da-b53a-45c97e350574-7174791f-9121-44fb-b006-3296806d4c39-e2a1a879-ac2b-4846-8304-1bf1cfec111e.jpg",
    ],
    "iphone": [
        "https://images-na.ssl-images-amazon.com/images/I/81yZOQEC%2BNL._AC_SL1500_.jpg",
        "https://static.toiimg.com/photo/71064804/Apple-iPhone-11-Pro.jpg",
        "https://www.proximus.be/dam/jcr:576989b8-1e78-4f53-8593-7b294853b774/cdn/sites/iportal/images/products/device/a/apple-iphone-se-2020-64gb-white/detail/apple-iphone-se-2020-64gb-white-XS-2~2020-04-17-05-27-32~cache.png",
        "https://zdnet3.cbsistatic.com/hub/i/r/2019/09/20/c016254f-a4bc-4259-8ddc-5a3b5d913f5a/resize/1200x900/e0884a3769d51a6ecd92c6cb9722a12a/iphone-11.jpg",
        "https://dbrand.com/sites/default/files/images/dbm-structured-data-image/iphone-xs-skin-16x9.png",
        "https://hoanghamobile.com/Uploads/Originals/2016/09/08/201609081107459092_black.jpg",
        "https://cdn1.viettelstore.vn/images/Product/ProductImage/medium/282227484.jpeg",
    ],
    "ipad": [
        "https://cdn.tgdd.vn/Products/Images/522/202820/ipad-mini-79-inch-wifi-cellular-64gb-2019-gold-600x600.jpg",
        "https://i.pcmag.com/imagery/reviews/04Pl0gzG2hLLGkonGAsUMmb-12..v_1574731296.jpg",
        "https://images-na.ssl-images-amazon.com/images/I/71YHduDAsdL._AC_SL1500_.jpg",
        "https://i-cdn.phonearena.com/images/review/4785-image/Apple-iPad-Pro-2020-Review.jpg",
        "https://www.91-img.com/pictures/126355-v1-apple-ipad-2018-wifi-32gb-tablet-large-1.jpg",
        "https://cdn.vox-cdn.com/uploads/chorus_image/image/63382453/akrales_190318_3298_0088.0.jpg",
        "https://cnet4.cbsistatic.com/img/Q_uTld8Bg2Idgnl9iLGkHP-z890=/1200x675/2018/03/28/03f5d952-0ccb-4a2c-bd28-597db4c0985f/24-ipad-2018-education.jpg",
        "https://sh-mdn-as-images.apple.com/is/ipad-pro-12-select-cell-spacegray-202003_FMT_WHH",
    ],
}
FIELDS = ["gs", "ce", "ac"]

SOLR_URL = "http://localhost:8983/solr/v2"


def average(arr):
    if not arr:
        return 0
    return sum(arr) / len(arr)


def average_precision(relevances):
    revs = 0
    precisions = []
    for idx, re in enumerate(relevances):
        if not re:
            continue
        revs += 1
        precisions.append(revs / (idx + 1))

    return average(precisions)


def precision_at(relevances, k):
    return average(relevances[:k])

@ray.remote
def test(img, type, field):
    try:
        query = requests.get(f"{SOLR_URL}/lireq", params={"field": field, "url": img, "accuracy": 1, "rows": 10})
        query.raise_for_status()
        response = query.json()
        res = []
        for doc in response["response"]["docs"]:
            res.append(f"/{type}/" in doc["id"])
        return img, average_precision(res), precision_at(res, 10)
    except KeyError:
        return img, -1, -1


# @ray.remote
# def by_device(device, field):
#     device_aps = {
#         url: ap for url, ap in ray.get([test.remote(url, device, field) for url in IMAGES[device]]) if ap > -1
#     }
#     mAP = average(device_aps.values())
#     return device, mAP, device_aps


# @ray.remote
# def by_field(field):
#     dev_info = ray.get([by_device.remote(device, field) for device in IMAGES])
#     # info = {device: {"mAP": _mAP, "info": info} for device, _mAP, info in dev_info}
#     info = {device: _mAP for device, _mAP, info in dev_info}
#     mAP = average([x[1] for x in dev_info])
#     full = {"mAP": mAP, "devices": info}
#     return field, full

# if __name__ == "__main__":
#     ray.init()
#     field_info = ray.get([by_field.remote(field) for field in FIELDS])
#     res = {field: info for field, info in field_info}
#     print(json.dumps(res, indent=4))


if __name__ == "__main__":
    ray.init()
    benchmark = {}

    for field in FIELDS:
        results = ray.get([
            test.remote(url, device, field) for device in IMAGES for url in IMAGES[device]
        ])
        results = [i for i in results if i[1] >= 0]

        map_ = average([res[1] for res in results])
        prec = average([res[2] for res in results])
        benchmark[field] = {"mAP": map_, "precision_at_10": prec}

    print(json.dumps(benchmark, indent=4))
