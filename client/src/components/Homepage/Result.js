import React, { useState, useEffect } from 'react';
import ReactJson from 'react-json-view';
import { useRecoilValue, useRecoilState } from 'recoil';
import { TablePagination } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import TableView from './TableView';
import ResultSwitchPanel from './ResultSwitchPanel';
import JsonView from './JsonView';
import { queryParamState } from '../../recoil/atom';
import { resultQuery, modeState } from '../../recoil/selector';

const useStyles = makeStyles(() => ({
  pagination: {
    padding: 0,
    display: 'flex',
    justifyContent: 'flex-end',
  },
}));

export default ({ isJsonView, setIsJsonView }) => {
  const classes = useStyles();
  const result = useRecoilValue(resultQuery);

  const [queryParam, setQueryParam] = useRecoilState(queryParamState);
  const mode = useRecoilValue(modeState);

  if (mode === 'image') {
    setIsJsonView(false);
  }

  const { numFound } = result;
  const handleSwitchChange = (event) => {
    if (mode === 'image') {
      return;
    }
    setIsJsonView(event.target.checked);
    if (!event.target.checked) {
      setQueryParam({ ...queryParam, fl: '' });
    }
  };

  const handleChangePage = async (event, newPage) => {
    const newQueryParam = { ...queryParam };
    newQueryParam.page = newPage;
    setQueryParam(newQueryParam);
  };

  const handleChangeRowsPerPage = async (event) => {
    const newRowsPerPage = parseInt(event.target.value, 10);
    const newQueryParam = { ...queryParam };
    newQueryParam.rowsPerPage = newRowsPerPage;
    newQueryParam.page = 0;
    setQueryParam(newQueryParam);
  };

  const [resultContainer, setResultContainer] = useState(<></>);
  useEffect(() => {
    if (isJsonView) {
      setResultContainer(
        <JsonView>
          <ReactJson src={result} />
        </JsonView>
      );
    } else {
      setResultContainer(<TableView />);
    }
  }, [isJsonView, result, queryParam.fl]);

  return (
    <>
      {resultContainer}
      {numFound > 0 && (
        <>
          <TablePagination
            component="div"
            className={classes.pagination}
            rowsPerPageOptions={[5, 10, 25]}
            colSpan={3}
            count={numFound}
            rowsPerPage={queryParam.rowsPerPage}
            page={queryParam.page}
            SelectProps={{
              inputProps: { 'aria-label': 'rows per page' },
              native: true,
            }}
            onChangePage={handleChangePage}
            onChangeRowsPerPage={handleChangeRowsPerPage}
          />
          <ResultSwitchPanel
            isJsonView={isJsonView}
            handleSwitchChange={handleSwitchChange}
          />
        </>
      )}
    </>
  );
};
