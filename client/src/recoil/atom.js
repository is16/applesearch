import { atom } from 'recoil';

export const collectionState = atom({
  key: 'collectionState',
  default: 'v1',
});

export const queryParamState = atom({
  key: 'queryParamState',
  default: {
    query: '',
    page: 0,
    rowsPerPage: 10,
    fl: '',
  },
});
