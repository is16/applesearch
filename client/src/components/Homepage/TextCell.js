import React from 'react';
import { ListItem, ListItemText, Divider, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyle = makeStyles(() => ({
  textField: {
    fontWeight: 600,
    position: 'absolute',
    left: '170px',
  },
  primaryText: {
    fontWeight: 600,
    fontSize: '1.3rem',
    marginBottom: '0.5rem',
  },
}));

// TODO: Change table rows according to fl field
export default ({ row }) => {
  const style = useStyle();

  const handleClickItem = (id) => (event) => {
    event.preventDefault();
    window.open(`/detail/${id}`);
  };

  const items = {
    colors: 'Color',
    Camera: 'Camera',
    Built_in_Display: 'Built in Display',
    Storage_Capacity: 'Storage Capacity',
    price: 'Price',
    Apple_Model_No: 'Model No',
    Introduction_Date: 'Introduction Date',
  };

  const listForMacbook = ['price', 'Apple_Model_No', 'Introduction_Date'];
  const listForOther = ['colors', 'Camera', 'Storage_Capacity'];

  return (
    <React.Fragment key={row.id}>
      <ListItem
        button
        alignItems="flex-start"
        onClick={handleClickItem(row.id)}
        key={row.id}
      >
        <ListItemText
          classes={{
            primary: style.primaryText,
          }}
          primary={row.name}
          secondary={
            <>
              {listForOther.map((e, i) =>
                row[e] ? (
                  <Typography
                    component="span"
                    variant="body2"
                    color="textPrimary"
                    display="block"
                    gutterBottom
                  >
                    {items[e]} :
                    <span className={style.textField}>{row[e]}</span>
                  </Typography>
                ) : (
                  <Typography
                    component="span"
                    variant="body2"
                    color="textPrimary"
                    display="block"
                    gutterBottom
                  >
                    {items[listForMacbook[i]]} :
                    <span className={style.textField}>
                      {row[listForMacbook[i]]}
                    </span>
                  </Typography>
                )
              )}
            </>
            // <>
            //   <Typography
            //     component="span"
            //     variant="body2"
            //     color="textPrimary"
            //     display="block"
            //     gutterBottom
            //   >
            //     Colors: <span className={style.textField}>{row.colors}</span>
            //   </Typography>
            //   <Typography
            //     component="span"
            //     variant="body2"
            //     color="textPrimary"
            //     display="block"
            //     gutterBottom
            //   >
            //     Camera: <span className={style.textField}>{row.Camera}</span>
            //   </Typography>
            //   <Typography
            //     component="span"
            //     variant="body2"
            //     color="textPrimary"
            //     display="block"
            //     gutterBottom
            //   >
            //     Display:{' '}
            //     <span className={style.textField}>{row.Built_in_Display}</span>
            //   </Typography>
            //   <Typography
            //     component="span"
            //     variant="body2"
            //     color="textPrimary"
            //     display="block"
            //     gutterBottom
            //   >
            //     Storage:{' '}
            //     <span className={style.textField}>{row.Storage_Capacity}</span>
            //   </Typography>
            // </>
          }
        ></ListItemText>
      </ListItem>
      <Divider />
    </React.Fragment>
  );
};
