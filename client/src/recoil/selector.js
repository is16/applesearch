import { selector } from 'recoil';
import { getDataText, getDataImage } from 'api/solr';
import { imageUrlRegex } from 'constants/index';
import { queryParamState } from './atom';

export const modeState = selector({
  key: 'modeState',
  get: ({ get }) => {
    const queryParam = get(queryParamState);
    const textInput = queryParam.query;
    if (imageUrlRegex.test(textInput)) return 'image';
    return 'text';
  },
});

export const resultQuery = selector({
  key: 'resultQuery',
  get: async ({ get }) => {
    const queryParam = get(queryParamState);
    // const collection = get(collectionState);
    const mode = get(modeState);
    try {
      let result;
      if (mode === 'text') {
        result = await getDataText('v1', { ...queryParam });
      } else {
        result = await getDataImage('v2', { ...queryParam });
      }
      return result.response;
    } catch (e) {
      console.log(e);
    }
    return {
      numFound: 0,
      start: 0,
      docs: [],
    };
  },
});
