# Crawling with Nutch

## Requirement

- Apache Nutch 1.16

- Apache Solr 8.5

## Install

- Copy file đã download dựa theo cấu trúc thư mục.

- Thay đổi Solr URL trong trường `org.apache.nutch.indexwriter.solr.SolrIndexWriter` trong file `${NUTCH_DIR}/conf/index-writers.xml`.

## Crawling

- Thêm trang khởi đầu hoặc sitemap vào file `urls/seed.txt`

- Định nghĩa Regex cho các trang sẽ được crawl (tính cả sitemap nếu có) trong `${NUTCH_DIR}/conf/regex-urlfilter.txt`.

- Định nghĩa các trường sẽ chọn và CSS selector trong file `${NUTCH_DIR}/conf/extractors.xml` dựa vào https://github.com/BayanGroup/nutch-custom-search.

- Thay đổi các config khác của Apache Nutch (nếu cần):

  - Loại bỏ field: Thêm trường cần loại bỏ vào `org.apache.nutch.indexwriter.solr.SolrIndexWriter` > `remove` trong file `${NUTCH_DIR}/conf/index-writers.xml`.

  - Loại bỏ doc dựa theo field: Định nghĩa JEXL expression để loại bỏ doc trong file `${NUTCH_DIR}/conf/nutch-site.xml`.
  ```xml
  <property>
    <name>index.jexl.filter</name>
    <value>url.matches(".*apple.*) || parseMeta.categories_concat.matches("(?i).*\\b(?:ios|swift)\\b.*")</value>
    <!-- object in Metadata context may have type T or [T], such a bullshit -->
    <description> A JEXL expression. If it evaluates to false,
    the document will not be indexed.
    Available primitives in the JEXL context:
    * status, fetchTime, modifiedTime, retries, interval, score, signature, url, text, title
    Available objects in the JEXL context:
    * httpStatus - contains majorCode, minorCode, message
    * documentMeta, contentMeta, parseMeta - contain all the Metadata properties.
    * doc - contains all the NutchFields from the NutchDocument.
      each property value is always an array of Objects.
    </description>
  </property>
  ```

- Bắt đầu quá trình crawl:
  - Nếu URL trong `urls/seed.txt` là sitemap: chạy `${NUTCH_HOME}/bin/crawl -i -sm urls crawl ${repeatTime}`
  - Nếu không, chạy `${NUTCH_HOME}/bin/crawl -i -s urls crawl ${repeatTime}`

## Something

- Nếu bị lỗi 403: Thêm vào `conf/nutch-site.xml`:
  ```xml
  <property>
    <name>http.agent.name</name>
    <value>Googlebot</value>
  </property>
  <property>
    <name>http.agent.version</name>
    <value>2.1</value>
  </property>
  <property>
    <name>http.agent.url</name>
    <value>http://www.google.com/bot.html</value>
  </property>
  ```

- Nếu bị thiếu trường:
  + Nếu có Indexing job did not succeed, job status:FAILED, reason: NA -> Xem logs
  + 