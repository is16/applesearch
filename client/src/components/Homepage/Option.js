import React from 'react';
import {
  FormControl,
  Select,
  Slider,
  Grid,
  InputLabel,
  TextField,
} from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';
import { useRecoilState, useRecoilValue } from 'recoil';
import { queryParamState } from 'recoil/atom';
import { modeState } from 'recoil/selector';
import { debounce } from 'lodash';
import { lireFieldOptions } from '../../constants/index';

const marks = [
  {
    value: 0.05,
    label: 0.05,
  },
  {
    value: 1,
    label: 1,
  },
];

const valueText = (value) => value;

const useStyles = makeStyles((theme) => ({
  container: {
    'padding-bottom': theme.spacing(4),
  },
  slider: {
    'padding-top': theme.spacing(2),
  },
}));

export default ({ setDescriptor, setAccuracy, isJsonView }) => {
  const classes = useStyles();
  const [queryParam, setQueryParam] = useRecoilState(queryParamState);
  const mode = useRecoilValue(modeState);

  const handleChangeDescriptor = (event) => {
    setDescriptor(event.target.value);
    debounceSearch({ descriptor: event.target.value });
  };

  const handleChangeAccuracy = (event, newValue) => {
    setAccuracy(newValue);
    debounceSearch({ accuracy: newValue });
  };

  const handleFieldListChange = (event) => {
    debounceSearch({ fl: event.target.value });
  };

  const debounceSearch = debounce((query) => {
    const newQueryParam = {
      ...queryParam,
      ...query,
      page: 0,
      rowsPerPage: 10,
    };
    setQueryParam(newQueryParam);
  }, 500);

  return (
    <>
      {(mode !== 'text' || isJsonView) && <h3>Options</h3>}
      {mode === 'text' && isJsonView && (
        <Grid
          container
          direction="row"
          justify="flex-start"
          alignItems="center"
          className={classes.container}
        >
          <Grid item xs={2}>
            <span>Field List: </span>
          </Grid>
          <Grid item xs>
            <FormControl variant="outlined" fullWidth>
              <InputLabel htmlFor="outlined-age-native-simple" />
              <TextField
                id="outlined-basic"
                // label="Field List"
                placeholder="eg. name, price..."
                variant="outlined"
                onChange={handleFieldListChange}
              />
            </FormControl>
          </Grid>
        </Grid>
      )}
      {mode === 'image' && (
        <Grid
          container
          direction="row"
          justify="flex-start"
          alignItems="center"
          className={classes.container}
        >
          <Grid item xs={2}>
            <span>Descriptor: </span>
          </Grid>
          <Grid item xs>
            <FormControl variant="outlined" fullWidth>
              <InputLabel htmlFor="outlined-age-native-simple">
                Descriptor
              </InputLabel>
              <Select
                native
                defaultValue="ce"
                onChange={handleChangeDescriptor}
                label="Descriptor"
                inputProps={{
                  name: 'descriptor',
                  id: 'outlined-age-native-simple',
                }}
              >
                {lireFieldOptions.map((e) => (
                  <option key={e.field} value={e.field}>
                    {e.label}
                  </option>
                ))}
              </Select>
            </FormControl>
          </Grid>
        </Grid>
      )}
    </>
  );
};
