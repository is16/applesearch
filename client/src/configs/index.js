import deepFreeze from 'deep-freeze';

const baseConfig = {
  appName: 'react_app_template',
  storagePrefix: 'react-example-',
};

// const env = process.env.REACT_APP_ENV;
const envConfig = {};
// if (env === 'development') {
//   envConfig.apiUrl = 'http://192.168.1.x:8983/solr';
// } else if (env === 'production') {
//   envConfig.apiUrl = 'http://3.87.95.140:8983/solr';
// } else {
//   envConfig.apiUrl = 'http://localhost:8983/solr';
// }
envConfig.apiUrl = 'http://localhost:8983/solr';

const configs = {
  ...baseConfig,
  ...envConfig,
};

deepFreeze(configs);

export default configs;
