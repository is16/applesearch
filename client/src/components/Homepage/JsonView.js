import React from 'react';

import styled from 'styled-components';

const JsonViewWrapper = styled.div`
  height: 600px;
  overflow-y: auto;
  border: 1px solid lightblue
`;

export default ({ children }) => (
  <JsonViewWrapper>
    {children}
  </JsonViewWrapper>
);
