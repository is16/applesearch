import React from 'react';
import { ListItem, ListItemText, Divider, Typography } from '@material-ui/core';
import { makeStyles } from '@material-ui/core/styles';

const useStyle = makeStyles(() => ({
  image: {
    maxWidth: '150px',
  },
  textField: {
    fontWeight: 600,
    position: 'absolute',
    left: '170px',
  },
  img: {
    width: '60%',
    height: 'auto',
  },
  primaryText: {
    fontWeight: 600,
    fontSize: '1.3rem',
    marginBottom: '0.5rem',
  },
}));

// TODO: Fix image URL
export default ({ row }) => {
  const style = useStyle();

  const handleClickItem = (id) => (event) => {
    event.preventDefault();
    window.open(`/detail/${id}`);
  };
  return (
    <>
      <ListItem
        button
        alignItems="flex-start"
        onClick={handleClickItem(row.id)}
      >
        <ListItemText
          classes={{
            primary: style.primaryText,
          }}
          primary={row.name}
          secondary={
            <>
              <Typography
                component="span"
                variant="body2"
                color="textPrimary"
                display="block"
                gutterBottom
              >
                Color: <span className={style.textField}>{row.color}</span>
              </Typography>
              <Typography
                component="span"
                variant="body2"
                color="textPrimary"
                display="block"
                gutterBottom
              >
                Category:{' '}
                <span className={style.textField}>{row.category}</span>
              </Typography>
              <Typography
                component="span"
                variant="body2"
                color="textPrimary"
                display="block"
                gutterBottom
              >
                Release date:{' '}
                <span className={style.textField}>{row.release_date}</span>
              </Typography>
              <Typography
                component="span"
                variant="body2"
                color="textPrimary"
                display="block"
                gutterBottom
              >
                Distance:{' '}
                <span className={style.textField}>{row.d.toFixed(4)}</span>
              </Typography>
            </>
          }
        ></ListItemText>
        <img className={style.image} src={`/${row.id}`} alt="" />
      </ListItem>
      <Divider />
    </>
  );
};
