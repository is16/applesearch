import { get } from 'utils/request';
import configs from 'configs';

export function getDataText(
  collection,
  { query, rowsPerPage, page, descriptor, accuracy, fl }
) {
  // Ensure id field is returned
  const queryParams = {
    q: query,
    rows: rowsPerPage || 10,
    start: page * rowsPerPage || 0,
    descriptor,
    accuracy,
  };
  if (fl && fl.length > 0) {
    queryParams.fl = `id,${fl}`;
  }
  return get(`${configs.apiUrl}/${collection}/select`, queryParams);
}

export function getDataImage(
  collection,
  { query, rowsPerPage, page, descriptor, accuracy, fl }
) {
  const queryParams = {
    url: query,
    rows: rowsPerPage || 10,
    start: page * rowsPerPage || 0,
    field: descriptor,
    fl:
      fl && fl.length > 0 ? `id:${fl}` : 'id,name,color,category,release_date',
  };
  return get(`${configs.apiUrl}/${collection}/lireq`, queryParams);
}
