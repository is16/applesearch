export const lireFieldOptions = [
  {
    field: 'gs',
    label: 'SIFT',
  },
  {
    field: 'ce',
    label: 'CEDD (very compact and accurate joint descriptor)',
  },
  {
    field: 'ac',
    label: 'AutoColorCorrelogram (color to color correlation histogram)',
  },
];

export const imageUrlRegex = /(jpg|gif|png|jpeg|webp)$/i;

export const collectionList = ['v1', 'v2'];
export const mockData = {
  response: {
    numFound: 5,
    start: 0,
    docs: [
      {
        id: '1',
        name: 'iPhone 6',
        colors: ['Black'],
        Camera: '11MP',
        Built_in_Display: 'AMOLED',
        Storage_Capacity: '64GB',
        image:
          'https://cdn.tgdd.vn/Products/Images/42/60546/iphone-5s-16gb-13-300x300.jpg',
      },
      {
        id: '2',
        name: 'iPhone 6',
        colors: ['Black'],
        Camera: '11MP',
        Built_in_Display: 'AMOLED',
        Storage_Capacity: '64GB',
        image:
          'https://cdn.tgdd.vn/Products/Images/42/60546/iphone-5s-16gb-13-300x300.jpg',
      },
      {
        id: '3',
        name: 'iPhone 6',
        colors: ['Black'],
        Camera: '11MP',
        Built_in_Display: 'AMOLED',
        Storage_Capacity: '64GB',
        image:
          'https://cdn.tgdd.vn/Products/Images/42/60546/iphone-5s-16gb-13-300x300.jpg',
      },
      {
        id: '4',
        name: 'iPhone 6',
        colors: ['Black'],
        Camera: '11MP',
        Built_in_Display: 'AMOLED',
        image:
          'https://cdn.tgdd.vn/Products/Images/42/60546/iphone-5s-16gb-13-300x300.jpg',
        Storage_Capacity: '64GB',
      },
      {
        id: '5',
        name: 'iPhone 6',
        colors: ['Black'],
        Camera: '11MP',
        Built_in_Display: 'AMOLED',
        image:
          'https://cdn.tgdd.vn/Products/Images/42/60546/iphone-5s-16gb-13-300x300.jpg',
        Storage_Capacity: '64GB',
      },
      {
        id: '6',
        name: 'iPhone 6',
        colors: ['Black'],
        Camera: '11MP',
        Built_in_Display: 'AMOLED',
        image:
          'https://cdn.tgdd.vn/Products/Images/42/60546/iphone-5s-16gb-13-300x300.jpg',
        Storage_Capacity: '64GB',
      },
    ],
  },
};
